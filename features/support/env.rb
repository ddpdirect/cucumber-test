require 'capybara/cucumber'

Capybara.run_server = false


Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

#Capybara.default_driver = :selenium
Capybara.default_driver = :chrome
