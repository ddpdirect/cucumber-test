Given(/^I am on the Google page$/) do
    visit 'http://www.google.com'
end

Then(/^on Google I search for "(.*?)"$/) do |arg1|
    fill_in 'lst-ib', with: arg1
end
 
 
Then(/^Stop$/) do
  save_and_open_page
end


#############


Given(/^I am on the YouTube home page$/) do
     visit 'http://www.youtube.com'
end

#When(/^I search for "(.*?)"$/) do |search|
#     fill_in 'search_query', :with => search
#     click_on 'search-btn'     
#end

#Then(/^"([^"]*)" should be found$/) do |findme|
Then(/^"([^"]*)" should be (?:found|seen|read|on the page)$/) do |findme|
  expect(page).to have_content findme
end



#############

# Bayernwerke
Then(/^I search for "([^"]*)" on MND Unternehmenssuche$/) do |mndpressroomsearch|
  fill_in 'query', with: mndpressroomsearch
  click_button('query')

# page.find('button').click
###### das findet wenigstens überhaupt was




#  within('input#query').find('button').click

# within('button') do
#   click_link("button", :match => :first)
# end

  ####der browser-selektor: .input-group-btn > button:nth-child(1)
  
  # page.find(within('input#query')).click
  
#page.find(:xpath, '//button[contains(., "btn-default")]').click
### 

  #within'.input-group' do
  #click_on "button"
  #end

  #within "query" do
  #  click_on "button"
  #end

  # first(:button, "btn-default").click

# find(:xpath, "btn-default").click
# find(:xpath, '//button[@class="btn-default"]').click

end

Then(/^I click on first search result$/) do
  click_link('search-pressroom', match: :first )
  #click_link('pressroom article-list container', match: :first )
end


