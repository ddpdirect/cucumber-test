Feature: MND Kontakte hinzufügen
	Scenario: Kontakte per copy&paste einfügen

	  Given Browser has size x="1400" y="800"
		Given I am on MND logged in as "systemtester" with password "systemtester"
		Then I click on "Codemocracy"
		Then I choose "de" newsroom

# 	Then I click on "Network"
#	 	Then I click on "Kontakte"
#	 	Then I click on "Kontakte hinzufügen"
#  	Then I click on "Kontakte als Text einfügen"
		Then I navigate to Kontakte als Text einfügen
#		= shortcut for previous 4 steps

  	Then I paste "checkadoo@mo.fo" in Kontakte hier einfügen
  	Then I click on button "Weiter"
  	Then I choose list "Garten"
  	Then I scroll down to page end
  	Then I click on button "Weiter"
  	Then I click on "Okay"
  	Then I click on "Kontaktlisten"
  	Then I click on "Garten"

  	Then I should see "checkadoo@mo.fo" 
  	# assuming it has to be the first entry since default sorting is last = first

  	Then I choose second list
  	# assuming it has to be the 2nd checkbox since 1st is all contacts, 2nd ist last entry

  	Then I click on button Gewählte Kontakte löschen
  	Then I click on button "Bestätigen"
  	Then I click on button "Okay"
  	Then I should NOT see "checkadoo@mo.fo"
  	Then I click on "Kontakte hinzufügen"
  	Then I click on delete import file and confirm
 		# assuming it has to be the first entry since last import is shown first

  	Then Halt