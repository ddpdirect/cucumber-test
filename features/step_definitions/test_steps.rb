Given(/^Browser has size x="([^"]*)" y="([^"]*)"$/) do |x, y|
  Capybara.page.current_window.resize_to(x, y)
end
 
Given(/^I am on MND logged in as "([^"]*)" with password "([^"]*)"$/) do |user, pass|
  visit("https://www.mynewsdesk.com/user/signin")
  fill_in("username", with: user)
  fill_in('password', with: pass)
  click_button("login-button")
end

Given(/^I choose "([^"]*)" newsroom$/) do |newsroom|
  first(".site-#{newsroom}").click
  expect(page).to have_content("Mynewsdesk")
end


Then(/^I click on "([^"]*)"$/) do |clickon|
  click_link(clickon, match: :first )
  # click_link(clickon, { :match => :first } )
end

Then(/^I click on Datei wählen and feed it with arb\.xlsx$/) do
	require 'pry'
	binding.pry
	# = cucumber action replay


#expect(page).to have_content("Datei")
# # # nur nötig im Firefox - Wirkung unklar, da expect nicht wartet, sondern nut true oder false (?)

#page.driver.browser.all(:xpath, '//*[@id="file-select"]').last.send_keys('/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')
# # # macht weig. was es soll, aber nach Klick auf weiter kommt fehler - es scheint, als ob die Datei zwar als angehängt angezeigt, aber in wirklichkeit nicht hochgeladen wird

#attach_file("file-select", '/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')
#page.attach_file("file-select", '/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')
# # # ElementNotFound: Unable to find file field "file-select"

#page.attach_file(:xpath '//*[@id="new_network_import_type_file_upload"]/div[1]/div/div/div[2]/div/span/span' , '/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')
# # # unkorrekte verwendung xpath

#attach_file(page.driver.browser.all(:xpath, '//*[@id="file-select"]').last , '/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')
# # # ElementNotFound: Unable to find file field [#<Selenium::WebDriver::Element:0x..fcc6d00f7159bee88 id="0.5476292781312957-1">]
# # # from /Users/mndmitarbeiter/.rbenv/versions/2.2.5/lib/ruby/gems/2.2.0/gems/capybara-2.7.1/lib/capybara/node/finders.rb:44:in `block in find'

# page.driver.browser.all(:xpath, '//*[@id="file-select"]').last.send_keys('/Users/mndmitarbeiter/code/cucumber-test/upload/arb.xlsx')

# als probe im action replay:
#find("file-select", visible: false).click
#Unable to find css "file-select"

###general issue: absolute path atm	
end

Then(/^I click on button "([^"]*)"$/) do |button_content|
#	expect(page).to have_content(button_content)
    click_button(button_content)	
end


Then (/^pry$/) do
  require 'pry'
  binding.pry
end


Then (/^Halt$/) do
  ask('Stopping... press key to close browser')
end


Then(/^I paste "([^"]*)" in Kontakte hier einfügen$/) do |fill|
  fill_in("network_import_type_paste_emails", with: fill)
end


Then(/^I choose list "([^"]*)"$/) do |list|
#  find(:xpath, "(//input[@type='checkbox'])[#{i}]").click
#  klickt die x-te Checkbox an

  find('label', text:list ).click
end


Then(/^I scroll down to page end$/) do
  execute_script('window.scroll(0,1000);')
end

Then(/^I should see "(.*?)"$/) do |content|
  expect(page).to have_content(content)
end

Then(/^I choose second list$/) do
  find(:xpath, "(//input[@type='checkbox'])[2]").click
end

Then(/^I click on button Gewählte Kontakte löschen$/) do
  click_button("bulk-destroy")
end


Then(/^I should be on the page Alle Kontakte$/) do
  expect(page.current_url).to match("https://publish.mynewsdesk.com/de/network#contacts/all")
end



Then(/^I should NOT see "(.*?)"$/) do |nocontent|
  expect(page).to have_no_content(nocontent)
end

Then(/^I navigate to Kontakte als Text einfügen$/) do
  visit ("http://publish.mynewsdesk.com/de/network/import_type/pastes/new")
end

Then(/^I click on delete import file and confirm$/) do
	find(:xpath, '//*[@id="NetworkImportIndexApp-react-component-0"]/div/table/tbody/tr[1]/td[3]/div/div/button').click
	page.driver.browser.switch_to.alert.accept
end